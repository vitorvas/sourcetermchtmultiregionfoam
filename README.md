  # sourceTermChtMultiRegionFoam
  
  This file is a modified version of the original **OpenFOAM** code by
  *Vitor Vasconcelos Araújo Silva*, technologist
  of **Centro de Desenvolvimento da Tecnologia Nuclear, CDTN/CNEN, Brazil**
  
  
  contact: vitors@cdtn.br

  ---------------------------------------------------------------------------  
  CDTN/CNEN and hereby disclaims all copyright in the present
  program written by Vitor Vasconcelos Araújo Silva.
  
  Vitor Vasconcelos Araújo Silva, 17 April 2019
  
  Application
  -----------

  sourceTermChtMultiRegionFoam

  Description
  -----------

  A version of chtMultiRegionFoam adapted to get the source-term for 
  the energy equation from an external source. The source term is defined as a 
  volScalarField and read from a C array structure. This version is implemented 
  and tested to run in parallel and sequentialy.

  The reason for not using the default fvOptions to add a source-term is due to 
  the fact that there is no easy way to add a non-Uniform scalar field to fvOptions
  to represent the volumetric source term. An added benefit is to have the source term 
  filed accessible from paraview for post-processing purposes.

